import {taskStoreService} from '@/core/service/task_service/task-store-manager';
import {useTasksStore} from '@/core/stores/taskStore';
import {TasksServiceController} from '@/core/service/task_service/store-safer'
import {TaskEditor} from '@/core/service/task_service/edit-task-in-store'





// task service and utills
export const taskStoreActionSafer = new TasksServiceController()
export const taskServiseMemory = new taskStoreService(useTasksStore, taskStoreActionSafer)
export const taskParamsEditor = new TaskEditor(useTasksStore)