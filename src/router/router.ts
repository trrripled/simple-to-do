import HomeView from '@/views/HomeView.vue';
import Projects from '@/views/Projects.vue';
import {createWebHistory} from 'vue-router';


export const routes = {
    history: createWebHistory(),
    routes : [

        {
            path:'/home',
            component: HomeView
        },

        
        {
            path:'/pr',
            component: Projects
        }


    ]


}