
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import naive from "naive-ui";
import App from './App.vue'
import {routes} from './router/router'
import { createRouter } from 'vue-router'
import "./assets/scss/app.scss";

const app = createApp(App)



const router = createRouter(routes)
app.use(router)
app.use(naive)
app.use(createPinia())
app.mount('#app')
