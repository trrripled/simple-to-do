import type {StoreDefinition,_GettersTree,Store} from 'pinia';
import type{taskState, Tasks} from '@/core/stores/taskStore';
import type {Task} from '@/core/dto/task';




// taskStore typing


type taskStoreActions = {

    addTask(id: string, task: Task): void
    dropTask(id:string):void
    resetStoreState():void
    getAllTasks():Tasks
    setupNewTasks(newTasks:Tasks):void
}

export type typeTaskStore = StoreDefinition<string,taskState, {}, taskStoreActions>