import {defineStore} from 'pinia';
import type {Task} from '@/core/dto/task';



export type Tasks = {
        [id:string] : Task 
}

export type taskState  = {
    tasks : Tasks
}



export const useTasksStore = defineStore('taskMemory', {

    state:():taskState => {
        return {
            tasks : {}
        }
    },

    actions : {

        addTask(id:string, task:Task):void{
            this.tasks[id] = task
        },

        dropTask(id:string):void{
            delete this.tasks[id]
        },

        resetStoreState():void{
            this.$reset()
        },

        getAllTasks():Tasks{
            return this.tasks;
        },

        setupNewTasks(newTasks:Tasks):void{

            this.tasks = newTasks
        }


    }

})



