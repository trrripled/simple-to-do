import type { typeTaskStore } from '@/core/stores/storesTypes';
import type {Task} from '@/core/dto/task';




export class TaskEditor{

    private storeOfTasks : typeTaskStore


    constructor(taskStore : typeTaskStore){

        this.storeOfTasks = taskStore

    }

    public changeTableOfTask(taskId:string, newTable:string ):void{

        const store = this.storeOfTasks()
        const neededTaskObject = store.getAllTasks()

        neededTaskObject[taskId].table_of_task = newTable

    }
}