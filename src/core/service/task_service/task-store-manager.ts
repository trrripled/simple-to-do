import type { Task } from '@/core/dto/task';
import type { typeTaskStore } from '@/core/stores/storesTypes';
import type {TasksServiceController} from '@/core/service/task_service/store-safer'





export class taskStoreService {

    private idCouner : number
    private storeOfTask : typeTaskStore
    private actionCheker : TasksServiceController

    constructor(taskStore : typeTaskStore, actionSafer:TasksServiceController){

        this.storeOfTask = taskStore
        this.actionCheker = actionSafer
        this.idCouner = 1

    }


    private upIdCouner():void{
        this.idCouner += 1;

    }

    private makeObjectToAdd(
        name:string, 
        emptyStatus:boolean,
        completeStatus:boolean, 
        renmaeStatus:boolean
    ):Task{


        const taskObjectToAdd : Task = {

            table_of_task : name,
            isEmpty : emptyStatus,
            isComplete : completeStatus,
            rename_mode : renmaeStatus

        }

        return taskObjectToAdd;

    }

    public addNewTask(taskName:string):boolean{

        const taskId : string = this.idCouner.toString()
        const taskFirstName : string = taskName
        const isEmptyTask : boolean = false
        const isCompleteTask : boolean = false
        const renameModeTask : boolean = false
        const store = this.storeOfTask()

        const canAddTask : boolean = this.actionCheker.checkNameOfTask(taskName)

        if (canAddTask){

            const taskNewObject:Task = this.makeObjectToAdd(
                taskFirstName,
                isEmptyTask,
                isCompleteTask,
                renameModeTask
            )
            
            store.addTask(taskId, taskNewObject)
            this.upIdCouner()

            return true;
        }

        return false;

    }

    public getAllTasks():Array<Task> | any{

        const store = this.storeOfTask()
        const tasksInStore = store.getAllTasks()
        


        const outArrayOfTasks = []

        for (let id in tasksInStore){

            const taskObjWithid = {
                taskID : id,
                taskName : tasksInStore[id].table_of_task
            }
            outArrayOfTasks.push(taskObjWithid)
        }

        return outArrayOfTasks;
    }


    public dropTaskByid(id:string):void{

        const store = this.storeOfTask()
        store.dropTask(id)

    }



}


