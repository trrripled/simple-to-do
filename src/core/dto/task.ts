

export type Task =  {

    table_of_task : string
    isEmpty : boolean
    isComplete : boolean
    rename_mode : boolean

}