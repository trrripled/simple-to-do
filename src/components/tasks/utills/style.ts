import type {Ref} from 'vue';


type ClearAnimation<T, U> = (refObject: T, time: U) => Promise<void>;

type TypeClearTaskNameAnimation = ClearAnimation<Ref<string>, number>;

export const reverseClearAnimation:TypeClearTaskNameAnimation = async (refObject, time) => {


    const wordForAnimation:string = refObject.value;
    const wordLength:number = wordForAnimation.length;

    for (let i = wordLength - 1; i>-1; i--){

        const newValue = wordForAnimation.slice(0,i)
        refObject.value = newValue
        await new Promise(resolve =>{
            setTimeout(resolve,time )
        })

    }


}